#!/usr/bin/python
# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import threading
import time

GPIO.setwarnings(False)
hinder = {"Front": 0.0, "Right": 0.0, "Left": 0.0} #障礙物的位置 

#定義超音波感測器類別
class Sensor(threading.Thread):
    def __init__(self,TRIG,ECHO,SensorNum):
        threading.Thread.__init__(self)
        GPIO.setmode(GPIO.BCM)
        self.SensorNum = SensorNum
        print "SensorNum:" +str(self.SensorNum)+" init"
        self.TRIG = TRIG
        self.ECHO = ECHO
        GPIO.setup(self.TRIG, GPIO.OUT)
        GPIO.setup(self.ECHO, GPIO.IN)
    def run(self):
        t = threading.currentThread()
        while getattr(t, "do_run", True):
            GPIO.output(self.TRIG, False)
            time.sleep(0.5)
            GPIO.output(self.TRIG, True)
            time.sleep(0.00001)
            GPIO.output(self.TRIG, False)
            start = time.time()
            while GPIO.input(self.ECHO) == 0:
                start = time.time()
            while GPIO.input(self.ECHO) == 1:
                end = time.time()
            if self.SensorNum == 1:
                hinder["Left"] = round((end - start) * 34000/2,1) # 在類別中也可存取全局變數    
            elif self.SensorNum == 2:
                hinder["Right"] = round((end - start) * 34000/2,1)
            elif self.SensorNum == 3:
                hinder["Front"] = round((end - start) * 34000/2,1)
            time.sleep(0.1) 
            print "hinder:"+str(hinder)
            


def send():
    while True:
        time.sleep(1)
        print "hello world"
        
def main():
        try:
            Left = Sensor(23,24,1) # GPIO 23/24  =  pin 16/18
            Left.start()
            Right = Sensor(16,20,2) # GPIO 16/20  =  pin 36/38
            Right.start()
            Front = Sensor(5,6,3) # GPIO 5/6  =  pin 29/31
            Front.start()
            send()
        except KeyboardInterrupt:
            pass
        finally:
            Left.do_run = False
            Right.do_run = False
            Front.do_run = False

if __name__ == "__main__":
    main()


